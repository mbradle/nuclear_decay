//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer and Michael Bojazi.
//  
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for useful utilities for the nuclear_decay project.
////////////////////////////////////////////////////////////////////////////////
  
#include "nuclear_decay_utilities.h"

//##############################################################################
// get_decay_matrices().
//##############################################################################

std::pair< WnMatrix *, WnMatrix * >
get_decay_matrices( Libnucnet__Net * p_net, double d_cutoff_decay_rate )
{

  Libnucnet__NetView * p_net_view;
  Libnucnet__Species * p_reactant, * p_product;
  double d_rate;
  std::pair< WnMatrix *, WnMatrix * > my_matrices;
  WnMatrix * p_work;
  gsl_vector * p_vector;
  size_t i;
  
  my_matrices.first =
    WnMatrix__new(
      Libnucnet__Nuc__getNumberOfSpecies( Libnucnet__Net__getNuc( p_net ) ),
      Libnucnet__Nuc__getNumberOfSpecies( Libnucnet__Net__getNuc( p_net ) )
    );

  p_work =
    WnMatrix__new(
      Libnucnet__Nuc__getNumberOfSpecies( Libnucnet__Net__getNuc( p_net ) ),
      Libnucnet__Nuc__getNumberOfSpecies( Libnucnet__Net__getNuc( p_net ) )
    );

  p_net_view =
    Libnucnet__NetView__new(
      p_net,
      "",
      S_DECAY_XPATH
    );

  nnt::reaction_list_t reaction_list =
    nnt::make_reaction_list(
      Libnucnet__Net__getReac( Libnucnet__NetView__getNet( p_net_view ) )
    );

  BOOST_FOREACH( nnt::Reaction reaction, reaction_list )
  {

    d_rate =
      Libnucnet__Reaction__computeRate(
        reaction.getNucnetReaction(),
        1.e-10,
        NULL
      );

    nnt::reaction_element_list_t reactant_list =
      nnt::make_reaction_nuclide_reactant_list(
        reaction.getNucnetReaction()
      );

    if( reactant_list.size() == 1 && d_rate > d_cutoff_decay_rate )
    {

      p_reactant =
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc( p_net ),
          Libnucnet__Reaction__Element__getName(
            (reactant_list.begin())->getNucnetReactionElement()
          )
        );

      WnMatrix__assignElement(
        p_work,
        Libnucnet__Species__getIndex( p_reactant ) + 1,
        Libnucnet__Species__getIndex( p_reactant ) + 1,
        d_rate
      );

      nnt::reaction_element_list_t product_list =
        nnt::make_reaction_nuclide_product_list(
          reaction.getNucnetReaction()
        );

      BOOST_FOREACH( nnt::ReactionElement product, product_list )
      {

        p_product =
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc( p_net ),
            Libnucnet__Reaction__Element__getName(
              product.getNucnetReactionElement()
            )
          );

        WnMatrix__assignElement(
          p_work,
          Libnucnet__Species__getIndex( p_product ) + 1,
          Libnucnet__Species__getIndex( p_reactant ) + 1,
          -d_rate *
             (double) Libnucnet__Species__getA( p_product ) /
             (double) Libnucnet__Species__getA( p_reactant )
        );

      }

    }

  }

  p_vector = WnMatrix__getDiagonalElements( p_work );

  for(
    i = 0;
    i < p_vector->size;
    i++
  )
  {

    if( WnMatrix__value_is_zero( gsl_vector_get( p_vector, i ) ) )
      WnMatrix__assignElement( my_matrices.first, i + 1, i + 1, 1. );

  }

  gsl_vector_free( p_vector );

  WnMatrix__addValueToDiagonals( p_work, 1.e-300 );

  my_matrices.second = WnMatrix__getTransferMatrix( p_work );

  WnMatrix__free( p_work );

  WnMatrix__scaleMatrix( my_matrices.second, -1. );

  return my_matrices;

} 

//##############################################################################
// convert_to_mass_fractions().
//##############################################################################

void
convert_to_mass_fractions(
  Libnucnet__Nuc * p_nuc,
  gsl_vector * p_vector
)
{

  nnt::species_list_t species_list = nnt::make_species_list( p_nuc );

  BOOST_FOREACH( nnt::Species species, species_list )
  {

    gsl_vector_set(
      p_vector,
      Libnucnet__Species__getIndex( species.getNucnetSpecies() ),
      gsl_vector_get(
        p_vector,
        Libnucnet__Species__getIndex( species.getNucnetSpecies() )
      ) * Libnucnet__Species__getA( species.getNucnetSpecies() )
    );

  }

}

//##############################################################################
// convert_to_abundances().
//##############################################################################

void
convert_to_abundances(
  Libnucnet__Nuc * p_nuc,
  gsl_vector * p_vector
)
{

  nnt::species_list_t species_list = nnt::make_species_list( p_nuc );

  BOOST_FOREACH( nnt::Species species, species_list )
  {

    gsl_vector_set(
      p_vector,
      Libnucnet__Species__getIndex( species.getNucnetSpecies() ),
      gsl_vector_get(
        p_vector,
        Libnucnet__Species__getIndex( species.getNucnetSpecies() )
      ) / Libnucnet__Species__getA( species.getNucnetSpecies() )
    );

  }

}
//##############################################################################
// decay_abundances().
//##############################################################################

void
decay_abundances( Libnucnet * p_nucnet, double d_decay_time, std::string debug )
{

  WnMatrix * p_matrix;
  std::vector<nnt::Zone> zone_vector;

  Libnucnet__setZoneCompareFunction(
    p_nucnet,
    (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
  );

  nnt::zone_list_t zone_list = nnt::make_zone_list( p_nucnet );

  BOOST_FOREACH( nnt::Zone zone, zone_list )
  {
    zone_vector.push_back( zone );
  }

  Libnucnet__Zone__updateNetView(
    zone_vector[0].getNucnetZone(),
    EVOLUTION_NETWORK,
    NULL,
    NULL,
    Libnucnet__NetView__copy(
      zone_vector[0].getNetView(
        "",
        S_DECAY_XPATH
      )
    )
  ); 

  Libnucnet__Zone__computeRates(
    zone_vector[0].getNucnetZone(),
    1.e-10,
    1.e-10
  );

  p_matrix =
    Libnucnet__Zone__computeJacobian( zone_vector[0].getNucnetZone() );

  #pragma omp parallel for schedule( dynamic, 1 )
    for( size_t i = 0; i < zone_vector.size(); i++ )
    {

      gsl_vector * p_vector =
        Libnucnet__Zone__getAbundances( zone_vector[i].getNucnetZone() );
          
      WnSparseSolve__Exp * p_exp_solver = WnSparseSolve__Exp__new();

      if( debug == "debug" ) WnSparseSolve__Exp__setDebug( p_exp_solver );

      WnSparseSolve__Exp__updateMaximumIterations( p_exp_solver, 100000 );

      gsl_vector * p_sol =
	WnSparseSolve__Exp__solve(
	  p_exp_solver,
	  p_matrix,
	  p_vector,
	  d_decay_time
	);

      if( !p_sol )
      {
	std::cerr << "Solution not found!" << std::endl;
	exit( EXIT_FAILURE );
      }

      Libnucnet__Zone__updateAbundances(
	zone_vector[i].getNucnetZone(),
	p_sol
      );

      gsl_vector_free( p_sol );
      
      if( debug == "debug" )
	std::cout <<
	  Libnucnet__Zone__getLabel(
	    zone_vector[i].getNucnetZone(),
	    1
	  ) << std::endl;

       gsl_vector_free( p_vector );

       WnSparseSolve__Exp__free( p_exp_solver );

    }

  zero_small_abundances( p_nucnet, 0. );

  WnMatrix__free( p_matrix );

} 

//##############################################################################
// decay_abundances().
//##############################################################################

void
decay_abundances( Libnucnet * p_nucnet, double d_decay_time )
{

  decay_abundances( p_nucnet, d_decay_time, "no" );

}

//##############################################################################
// push_abundances_to_daughters().
//##############################################################################

void
push_abundances_to_daughters(
  Libnucnet * p_nucnet,
  double d_cutoff_decay_rate
)
{

  std::pair< WnMatrix *, WnMatrix * > my_matrices;
  gsl_vector * p_a, * p_b, * p_c, * p_undecayed;
  double d_mass;
  size_t i;

  my_matrices =
    get_decay_matrices(
      Libnucnet__getNet( p_nucnet ),
      d_cutoff_decay_rate
    );

  Libnucnet__setZoneCompareFunction(
    p_nucnet,
    (Libnucnet__Zone__compare_function)
      nnt::zone_compare_by_first_label
  );

  nnt::zone_list_t zone_list = nnt::make_zone_list( p_nucnet );

  BOOST_FOREACH( nnt::Zone zone, zone_list )
  {

    p_a = Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

    convert_to_mass_fractions(
      Libnucnet__Net__getNuc( Libnucnet__getNet( p_nucnet ) ),
      p_a
    );

    p_b =
      gsl_vector_alloc(
        WnMatrix__get_gsl_vector_size( p_a )
      );

    gsl_vector_memcpy( p_b, p_a );

    p_undecayed =
      gsl_vector_alloc(
        WnMatrix__get_gsl_vector_size( p_a )
      );

    gsl_vector_memcpy( p_undecayed, p_a );

    for( i = 0; i < 1000; i++ )
    {

      p_c = WnMatrix__computeMatrixTimesVector( my_matrices.second, p_b );
      gsl_vector_add( p_a, p_c );
      gsl_vector_memcpy( p_b, p_c );
      gsl_vector_free( p_c );

      if( WnMatrix__value_is_zero( gsl_blas_dnrm2( p_b ) ) ) break;

    }

    p_c = WnMatrix__computeMatrixTimesVector( my_matrices.first, p_a );

    d_mass = 0.;
    for( i = 0; i < WnMatrix__get_gsl_vector_size( p_c ); i++ )
    {
      if( gsl_vector_get( p_c, i ) > 1.e-20 )
        d_mass +=
          gsl_vector_get( p_c, i ) -
          gsl_vector_get( p_undecayed, i );
    }

    zone.updateProperty(
      S_RADIOACTIVE_MASS_FRACTION,
      boost::lexical_cast<std::string>( d_mass )
    );

    convert_to_abundances(
      Libnucnet__Net__getNuc( Libnucnet__getNet( p_nucnet ) ),
      p_c
    );

    Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_c );

    gsl_vector_free( p_a );
    gsl_vector_free( p_b );
    gsl_vector_free( p_c );
    gsl_vector_free( p_undecayed );

  }

  WnMatrix__free( my_matrices.first );
  WnMatrix__free( my_matrices.second );

}

//##############################################################################
// zero_small_abundances().
//##############################################################################

void
zero_small_abundances(
  Libnucnet * p_nucnet,
  double d_threshold
)
{

  nnt::zone_list_t zone_list = nnt::make_zone_list( p_nucnet );

  nnt::species_list_t species_list =
    nnt::make_species_list(
      Libnucnet__Net__getNuc(
        Libnucnet__getNet( p_nucnet )
      )
    );

  BOOST_FOREACH( nnt::Zone zone, zone_list )
  {

    BOOST_FOREACH( nnt::Species species, species_list )
    {

      if(
        Libnucnet__Zone__getSpeciesAbundance(
          zone.getNucnetZone(),
          species.getNucnetSpecies()
        ) < d_threshold
      )
        Libnucnet__Zone__updateSpeciesAbundance(
          zone.getNucnetZone(),
          species.getNucnetSpecies(),
          0.
        );
    }

  }

}
