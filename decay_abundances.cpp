////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to decay input abundances over input time.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include "nuclear_decay_utilities.h"

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char **argv )
{

  Libnucnet *p_my_nucnet;
  std::string
    s_input_xml, s_zone_xml, s_decay_time, s_zone_xpath, s_output_xml, s_debug;

  //============================================================================
  // Check input.
  //============================================================================

  if( argc == 2 && strcmp( argv[1], "--example" ) == 0 )
  {
    std::cout << std::endl;
    std::cout << argv[0] << " ../nucnet-tools-code/data_pub/my_net.xml " <<
      "zone.xml 1.e5 \"[position() >= last() - 10]\" out.xml" <<
      std::endl << std::endl;
    exit( EXIT_FAILURE );
  }

  if( argc < 5 || argc > 7 )
  {
    fprintf(
      stderr,
      "\nUsage: %s xml_file zone_file decay_time zone_xpath out_file debug\n\n",
      argv[0]
    );
    fprintf(
      stderr,
      "  xml_file = input network or full libnucnet xml file\n\n"
    );
    fprintf(
      stderr,
      "  zone_file = zone xml file (optional if xml_file is full file)\n\n"
    );
    fprintf(
      stderr,
      "  decay_time = time over which to decay abundances\n\n"
    );
    fprintf(
      stderr,
      "  zone_xpath = xpath to select zones\n\n"
    );
    fprintf(
      stderr,
      "  out_file = output file\n\n"
    );
    fprintf(
      stderr,
      "  debug = set to \"debug\" for debugging output (optional)\n\n"
    );
    std::cout << "For an example usage, type " << std::endl << std::endl;
    std::cout << argv[0] << " --example" << std::endl << std::endl;
    return EXIT_FAILURE;
  }

  //============================================================================
  // Determine input.
  //============================================================================

  if( argc == 5 || ( argc == 6 && strcmp( argv[5], "debug" ) == 0 ) )
  {
     
    s_input_xml = argv[1];
    s_zone_xml = argv[1];
    s_decay_time = argv[2];
    s_zone_xpath = argv[3];
    s_output_xml = argv[4];
    if( argc == 6 )
      s_debug = "debug";
    else
      s_debug = "";

  }
  else
  {
     
    s_input_xml = argv[1];
    s_zone_xml = argv[2];
    s_decay_time = argv[3];
    s_zone_xpath = argv[4];
    s_output_xml = argv[5];
    if( argc == 7 )
      s_debug = "debug";
    else
      s_debug = "";

  }

  //============================================================================
  // Get data.
  //============================================================================

  p_my_nucnet = Libnucnet__new();

  Libnucnet__Net__updateFromXml(
    Libnucnet__getNet( p_my_nucnet ),
    s_input_xml.c_str(),
    "",
    ""
  );

  Libnucnet__Reac__updateFromXml(
    Libnucnet__Net__getReac( Libnucnet__getNet( p_my_nucnet ) ),
    "added_reactions.xml",
    ""
  );

  Libnucnet__assignZoneDataFromXml(
    p_my_nucnet,
    s_zone_xml.c_str(),
    s_zone_xpath.c_str()
  );

  try{
    boost::lexical_cast<double>( s_decay_time );
  }
  catch( const boost::bad_lexical_cast& e )
  {
    std::cerr << s_decay_time << " is an invalid decay time." << std::endl;
    throw e;
  }

  //============================================================================
  // Decay abundances.
  //============================================================================

  if( s_debug == "debug" )
    decay_abundances(
      p_my_nucnet,
      boost::lexical_cast<double>( s_decay_time ),
      s_debug
    );
  else
    decay_abundances(
      p_my_nucnet,
      boost::lexical_cast<double>( s_decay_time )
    );

  //============================================================================
  // Output.
  //============================================================================

  Libnucnet__setZoneCompareFunction(
    p_my_nucnet,
    (Libnucnet__Zone__compare_function)
      nnt::zone_compare_by_first_label
  );

  Libnucnet__writeToXmlFile( p_my_nucnet, s_output_xml.c_str() );

  //============================================================================
  // Clean up and exit.
  //============================================================================

  Libnucnet__free( p_my_nucnet );

  return EXIT_SUCCESS;

}
