////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute the decayed abundances; that is, this code
//!        decays all species with decay lifetime less than the cutoff
//!        lifetime to their daughters.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include "nuclear_decay_utilities.h"

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char **argv )
{

  Libnucnet *p_my_nucnet;
  std::string s_decay_time, s_output_xml;
  double d_cutoff_decay_rate;

  if( argc == 2 && strcmp( argv[1], "--example" ) == 0 )
  {
    std::cout << std::endl;
    std::cout << argv[0] << " ../nucnet-tools-code/data_pub/my_net.xml " <<
      "zone.xml 1.e5 \"[position() >= last() - 10]\" out.xml" <<
      std::endl << std::endl;
    exit( EXIT_FAILURE );
  }

  if( argc != 5 && argc != 6 )
  {
    fprintf(
      stderr,
      "\nUsage: %s xml_file zone_file lifetime zone_xpath out_file\n\n",
      argv[0]
    );
    fprintf(
      stderr,
      "  xml_file = input network or full libnucnet xml file\n\n"
    );
    fprintf(
      stderr,
      "  zone_file = zone file (optional if using full libnucnet xml)\n\n"
    );
    fprintf(
      stderr,
      "  lifetime = cutoff lifetime (seconds)\n\n"
    );
    fprintf(
      stderr,
      "  zone_xpath = xpath to select zones\n\n"
    );
    fprintf(
      stderr,
      "  out_file = output xml file\n\n"
    );
    std::cout << "For an example usage, type " << std::endl << std::endl;
    std::cout << argv[0] << " --example" << std::endl << std::endl;
    return EXIT_FAILURE;
  }

  p_my_nucnet = Libnucnet__new();

  Libnucnet__Net__updateFromXml(
    Libnucnet__getNet( p_my_nucnet ),
    argv[1],
    "",
    S_DECAY_XPATH
  );

  Libnucnet__Reac__updateFromXml(
    Libnucnet__Net__getReac( Libnucnet__getNet( p_my_nucnet ) ),
    "added_reactions.xml",
    ""
  );

  if( argc == 5 )
  {

    Libnucnet__assignZoneDataFromXml(
      p_my_nucnet,
      argv[1],
      argv[3]
    );

    s_decay_time = argv[2];
    s_output_xml = argv[4];

  }
  else
  {

    Libnucnet__assignZoneDataFromXml(
      p_my_nucnet,
      argv[2],
      argv[4]
    );

    s_decay_time = argv[3];
    s_output_xml = argv[5];

  }

  try{
    d_cutoff_decay_rate = 1. / boost::lexical_cast<double>( s_decay_time );
  }
  catch( const boost::bad_lexical_cast& e )
  {
    if( s_decay_time == "inf" )
    {
      d_cutoff_decay_rate = 0.;
    }
    else
    {
      std::cerr << s_decay_time << " is an invalid decay time." << std::endl;
      throw e;
    }
  }

  push_abundances_to_daughters( p_my_nucnet, d_cutoff_decay_rate );

  Libnucnet__writeToXmlFile( p_my_nucnet, s_output_xml.c_str() );

  Libnucnet__free( p_my_nucnet );

  return EXIT_SUCCESS;

}
