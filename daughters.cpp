////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute ultimate daughters of radioactive species.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <fstream>
#include <iostream>
#include <numeric>
#include <string>

#include <boost/config.hpp>
#include <boost/graph/directed_graph.hpp>
#include <boost/graph/property_iter_range.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/graph_utility.hpp>

#include <Libnucnet.h>
#include "nnt/auxiliary.h"

#include "nnt/iter.h"

//##############################################################################
// Types and defines.
//##############################################################################

enum edge_parent_reaction_t { edge_parent_reaction };

namespace boost
{
  BOOST_INSTALL_PROPERTY( edge, parent_reaction );
}

#define S_REAC_XPATH \
   "[ count( reactant ) = 1 and not( reactant = 'n' and product = 'h1') ]"

typedef
boost::adjacency_list <
  boost::listS,
  boost::vecS,
  boost::directedS,
  boost::property<
    boost::vertex_name_t, std::string,
      boost::property< boost::vertex_color_t, boost::default_color_type
    >
  >,
  boost::property<
    boost::edge_weight_t, double,
      boost::property< edge_parent_reaction_t, std::string
    >
  >
> Graph;

typedef boost::property_map <Graph, boost::vertex_name_t>::type name_map_t;

typedef boost::property_map <Graph, boost::vertex_name_t>::const_type
  const_name_map_t;

typedef boost::property_map <Graph, boost::edge_weight_t>::type weight_map_t;

typedef boost::property_map <Graph, boost::edge_weight_t>::const_type
  const_weight_map_t;

typedef boost::graph_traits<Graph>::vertex_descriptor my_vertex_t;

typedef boost::graph_traits<Graph>::edge_descriptor my_edge_t;

typedef std::map<std::string, double> distance_map;

typedef std::set< my_edge_t > edge_set_t;

//##############################################################################
// LoopFinder.  Find back edges (loops) in graph.
//##############################################################################

class LoopFinder : public boost::default_dfs_visitor
{
public:
  LoopFinder( edge_set_t& set_e ) : _set_e( set_e ) {}
  void back_edge( my_edge_t e, const Graph& g) const
  {
    const_name_map_t name_map = boost::get( boost::vertex_name, g );
    std::cout << "Back edge: (" <<
      name_map[boost::source( e, g )] << "," <<
      name_map[boost::target( e, g )] << ")" << std::endl;
    _set_e.insert( e );
  }
private:
  edge_set_t& _set_e;
};

//##############################################################################
// visit_vertices().  Recursively visit descendants.
//##############################################################################

void visit_vertices( my_vertex_t v, const Graph& g, distance_map& map_d )
{

  const_name_map_t name_map = boost::get( boost::vertex_name, g );
  const_weight_map_t weight_map = boost::get( boost::edge_weight, g );
  boost::graph_traits<Graph>::out_edge_iterator oi, oi_end;

  for(
    boost::tie( oi, oi_end ) = boost::out_edges( v, g );
    oi != oi_end;
    oi++
  )
  {
    double d = weight_map[*oi] * map_d[name_map[boost::source( *oi, g )]];
    if( boost::out_degree( boost::target( *oi, g ), g ) == 0 )
    {
      map_d[name_map[boost::target( *oi, g )]] += d;
    }
    else
    {
      map_d[name_map[boost::target( *oi, g )]] = d;
      visit_vertices( boost::target( *oi, g ), g, map_d );
    }
  }

  map_d.erase( name_map[v] );

}

//##############################################################################
// create_graph().
//##############################################################################

Graph
create_graph( Libnucnet__Net * p_net )
{

  Graph g;

  std::map< std::string, boost::graph_traits<Graph>::vertex_descriptor > map_v;

  boost::graph_traits<Graph>::vertex_descriptor v;
  boost::graph_traits<Graph>::edge_descriptor e;

  name_map_t name_map = boost::get( boost::vertex_name, g );

  boost::property_map <Graph, edge_parent_reaction_t>::type
    parent_reaction_map = boost::get( edge_parent_reaction, g );

  bool added;

  nnt::species_list_t species_list =
    nnt::make_species_list( Libnucnet__Net__getNuc( p_net ) );

  BOOST_FOREACH( nnt::Species species, species_list )
  {

    v = boost::add_vertex( g );

    name_map[v] = Libnucnet__Species__getName( species.getNucnetSpecies() );

    map_v[ Libnucnet__Species__getName( species.getNucnetSpecies() )] = v;

  }

  nnt::reaction_list_t reaction_list =
    nnt::make_reaction_list( Libnucnet__Net__getReac( p_net ) );

  BOOST_FOREACH( nnt::Reaction reaction, reaction_list )
  {

    if( Libnucnet__Net__isValidReaction( p_net, reaction.getNucnetReaction() ) )
    {

      nnt::reaction_element_list_t reactants =
        nnt::make_reaction_nuclide_reactant_list(
          reaction.getNucnetReaction()
        );

      nnt::reaction_element_list_t products =
        nnt::make_reaction_nuclide_product_list( reaction.getNucnetReaction() );

      BOOST_FOREACH( nnt::ReactionElement reactant, reactants )
      {

        v =
          map_v[
            Libnucnet__Reaction__Element__getName(
              reactant.getNucnetReactionElement()
            )
          ];

        BOOST_FOREACH( nnt::ReactionElement product, products )
        {

          boost::tie( e, added ) =
            boost::add_edge(
              v,
              map_v[
                Libnucnet__Reaction__Element__getName(
                  product.getNucnetReactionElement()
                )
              ],
              g
            );

          parent_reaction_map[e] =
            Libnucnet__Reaction__getString( reaction.getNucnetReaction() );

        }

      }

    }

  }

  return g;

}
    
//##############################################################################
// set_weights().
//##############################################################################

void
set_weights(
  Libnucnet__Net * p_net,
  Graph& g,
  double d_cutoff
)
{

  double d_forward, d_reverse;
  boost::graph_traits<Graph>::edge_iterator ei, ei_end, next;

  std::map< std::string, double > rate_map;

  nnt::reaction_list_t reaction_list =
    nnt::make_reaction_list( Libnucnet__Net__getReac( p_net ) );
 
  BOOST_FOREACH( nnt::Reaction reaction, reaction_list )
  {

    Libnucnet__Net__computeRatesForReaction(
      p_net,
      reaction.getNucnetReaction(),
      1.e-10,
      1.,
      NULL,
      &d_forward,
      &d_reverse
    );

    rate_map[Libnucnet__Reaction__getString( reaction.getNucnetReaction() )] =
      d_forward;

  }

  weight_map_t weight_map = boost::get( boost::edge_weight, g );

  boost::property_map <Graph, edge_parent_reaction_t>::type
    parent_reaction_map = boost::get( edge_parent_reaction, g );

  boost::tie( ei, ei_end ) = boost::edges( g );
  for( next = ei; ei != ei_end; ei = next )
  {

    ++next;
    double d_weight = rate_map[ parent_reaction_map[*ei] ];
    if( d_weight > d_cutoff )
      weight_map[ *ei ] = d_weight;
    else
      boost::remove_edge( *ei, g );

  }

}

void
set_weights(
  Libnucnet__Net * p_net,
  Graph& g
)
{
  set_weights( p_net, g, 0. );
}

//##############################################################################
// normalize_weights().
//##############################################################################

void
normalize_weights(
  Graph& g
)
{

  boost::graph_traits<Graph>::vertex_iterator vi, vi_end;
  boost::graph_traits<Graph>::out_edge_iterator oi, oi_end;

  std::set< std::string > my_set;

  boost::property_map <Graph, edge_parent_reaction_t>::type
    parent_reaction_map = boost::get( edge_parent_reaction, g );

  weight_map_t weight_map = boost::get( boost::edge_weight, g );

  for( boost::tie( vi, vi_end ) = boost::vertices( g ); vi != vi_end; vi++ )
  {

    my_set.clear();

    double d_w = 0;

    for(
      boost::tie( oi, oi_end ) = boost::out_edges( *vi, g );
      oi != oi_end;
      oi++
    )
    {

      std::pair< std::set< std::string >::iterator, bool > r =
        my_set.insert( parent_reaction_map[*oi] );
      
      if( r.second == true ) d_w += weight_map[*oi];

    }

    if( d_w > 0 )
    {
      for(
        boost::tie( oi, oi_end ) = boost::out_edges( *vi, g );
        oi != oi_end;
        oi++
      )
      {
        weight_map[*oi] /= d_w;
      }
    }

  }

}
 
//##############################################################################
// main().
//##############################################################################

int
main( int argc, char * argv[] ) {

  Libnucnet__Net * p_my_net;
  Graph g;
  boost::graph_traits<Graph>::vertex_iterator vi, vi_end;
  distance_map map_d;
  std::map< std::string, my_vertex_t > map_v;
  edge_set_t set_e;

  //============================================================================
  // Check input.
  //============================================================================

  if ( argc < 3 || argc > 4 ) {
      fprintf(
        stderr, "\nUsage: %s file nuc_xpath cutoff \n\n", argv[0]
      );
      fprintf(
        stderr, "  file = input xml filename\n\n"
      );
      fprintf(
        stderr,
        "  nuc_xpath = XPath expression to select nuclides\n\n"
      );
      fprintf(
        stderr,
        "  cutoff = rate cutoff (optional)\n\n"
      );

      return EXIT_FAILURE;
  }

  //============================================================================
  // Read input data.
  //============================================================================

  p_my_net = Libnucnet__Net__new_from_xml( argv[1], NULL, S_REAC_XPATH );

  //============================================================================
  // Get graph.
  //============================================================================

  g = create_graph( p_my_net );

  if( argc == 3 )
    set_weights( p_my_net, g );
  else
    set_weights( p_my_net, g, atof( argv[3] ) );

  //============================================================================
  // Remove back edges (loops).
  //============================================================================

  LoopFinder vis( set_e );

  boost::depth_first_search(
    g,
    boost::visitor( vis )
  );

  for( edge_set_t::iterator it = set_e.begin(); it != set_e.end(); it++ )
  {
    boost::remove_edge( *it, g );
  }

  //============================================================================
  // Normalize weights.
  //============================================================================

  normalize_weights( g );

  //============================================================================
  // Interior maps.
  //============================================================================

  name_map_t name_map = boost::get( boost::vertex_name, g );

  //============================================================================
  // Iterate view.
  //============================================================================

  for( boost::tie( vi, vi_end ) = boost::vertices( g ); vi != vi_end; vi++ )
  {
    map_v[ name_map[*vi] ] = *vi;
  }

  Libnucnet__NucView * p_view =
    Libnucnet__NucView__new(
      Libnucnet__Net__getNuc( p_my_net ),
      argv[2]
    );

  nnt::species_list_t species_list =
    nnt::make_species_list( Libnucnet__NucView__getNuc( p_view ) );

  BOOST_FOREACH( nnt::Species species, species_list )
  {
    my_vertex_t v =
      map_v.find(
        Libnucnet__Species__getName( species.getNucnetSpecies() )
      )->second;
    map_d.clear();
    map_d[ name_map[v] ] = 1.;
    visit_vertices( v, g, map_d );
    if( map_d.size() > 0 )
    {
      std::cout << name_map[v] << ":" << std::endl;
      for(
        distance_map::iterator it = map_d.begin();
        it != map_d.end();
        it++
      )
        std::cout << " " << it->first << " " << it->second << std::endl;
    }
  }

  //============================================================================
  // Clean up and exit.
  //============================================================================

  Libnucnet__NucView__free( p_view );

  Libnucnet__Net__free( p_my_net );

  return EXIT_SUCCESS;

}
