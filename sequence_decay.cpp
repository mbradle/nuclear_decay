////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to decay input abundances over multiple time periods.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include "user/hdf5_routines.h"

#include "my_user/my_times_function.h"

#include "nuclear_decay_utilities.h"

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char **argv )
{

  std::vector<double> times;

  //============================================================================
  // Get input.
  //============================================================================

  std::pair<Libnucnet *, std::map<std::string, std::string> > my_pair =
    my_user::nuclear_decay::get_input( argc, argv );

  //============================================================================
  // Get decay times.
  //============================================================================

  my_user::nuclear_decay::update_times_vector(
    my_pair.first, my_pair.second, times
  );

  //============================================================================
  // Insert initial time in zones.
  //============================================================================

  nnt::zone_list_t zone_list = nnt::make_zone_list( my_pair.first );

  BOOST_FOREACH( nnt::Zone zone, zone_list )
  {
    zone.updateProperty(
      nnt::s_TIME,
      boost::lexical_cast<std::string>( times[0] )
    );
  }

  //============================================================================
  // Create output and output initial step.
  //============================================================================

  user::hdf5::create_output(
    my_pair.second.find( S_OUTPUT_FILE )->second.c_str(),
    my_pair.first
  );

  user::hdf5::append_zones(
    my_pair.second.find( S_OUTPUT_FILE )->second.c_str(),
    my_pair.first
  );

  //============================================================================
  // Push abundances to daughters for 1 second.
  //============================================================================
  
  push_abundances_to_daughters(
    my_pair.first,
    1. /
      boost::lexical_cast<double>(
        my_pair.second.find( S_PUSH_TIME )->second
      )
  ); 

  //============================================================================
  // Loop over times and decay.
  //============================================================================

  for( size_t i = 1; i < times.size(); i++ )
  {

     std::cout << times[i] << std::endl;

    BOOST_FOREACH( nnt::Zone zone, zone_list )
    {
      zone.updateProperty(
        nnt::s_TIME,
        boost::lexical_cast<std::string>( times[i] )
      );
    }

    if( my_pair.second.find( S_DEBUG ) != my_pair.second.end() )
    {
      decay_abundances(
        my_pair.first,
        times[i] - times[i - 1],
        my_pair.second.find( S_DEBUG )->second
      );
    }
    else
    {
      decay_abundances(
        my_pair.first,
        times[i] - times[i - 1]
      );
    }

    user::hdf5::append_zones(
      my_pair.second.find( S_OUTPUT_FILE )->second.c_str(),
      my_pair.first
    );

  }

  //============================================================================
  // Clean up and exit.
  //============================================================================

  Libnucnet__free( my_pair.first );

  return EXIT_SUCCESS;

}
