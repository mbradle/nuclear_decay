#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2011-2013 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer and Michael J. Bojazi.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#///////////////////////////////////////////////////////////////////////////////

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate nuclear decay codes.
#//!
#///////////////////////////////////////////////////////////////////////////////


SVNURL = svn://svn.code.sf.net/p/nucnet-tools/code/trunk

NUCNET_TARGET = ../nucnet-tools-code

VENDORDIR = $(NUCNET_TARGET)/vendor
OBJDIR = $(NUCNET_TARGET)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user
MY_USER_DIR = $(NUCNET_TARGET)/my_user
BUILD_DIR = $(NUCNET_TARGET)/build

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# My user.
#===============================================================================

MY_USER := $(shell mkdir -p $(NUCNET_TARGET)/my_user)

MY_USER_COPY := $(shell if [ ! -f $(MY_USER_DIR)/my_times_function.cpp ]; then cp my_user_default/Makefile.inc $(MY_USER_DIR)/Makefile.inc; cp my_user_default/my_times_function.cpp $(MY_USER_DIR)/my_times_function.cpp; cp my_user_default/my_times_function.h $(MY_USER_DIR)/my_times_function.h; fi)

#===============================================================================
# Preliminaries.
#===============================================================================
 
include $(BUILD_DIR)/Makefile

include $(BUILD_DIR)/Makefile.sparse

include $(USER_DIR)/Makefile.inc

include $(MY_USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR):$(MY_USER_DIR)

DECAY_OBJS = $(WN_OBJ)        	\
             $(NNT_OBJ)		\
             $(SOLVE_OBJ)	\
             $(DECAY_OBJ)	\
             $(USER_OBJ)	\
             $(MY_USER_OBJ)	\
             $(SP_OBJ)

ifdef USE_HDF5
  GC=h5c++
  FF += -lhdf5 -lhdf5_cpp
  DECAY_OBJS += $(HD5_OBJ)
endif

FLIBS= -L$(SPARSKITDIR) -lskit -lstdc++

#===============================================================================
# Executables.
#===============================================================================

DECAY_EXEC = compute_decayed_abundances \
             decay_abundances \
             rad_vs_nucleon_number \
             daughters \
             compute_overabundances \
             compute_species_overabundances \

ifdef USE_HDF5
  DECAY_EXEC += sequence_decay
endif

$(DECAY_EXEC): sparse $(DECAY_OBJS)
	$(CC) -c -o $(OBJDIR)/$@.o $@.cpp
	$(FF) $(DECAY_OBJS) $(OBJDIR)/$@.o -o $(BINDIR)/$@ $(CLIBS) $(FLIBS)

.PHONY all_nuclear_decay : $(DECAY_EXEC)

#===============================================================================
# Clean up.
#===============================================================================

.PHONY: clean_nuclear_decay cleanall_nuclear_decay \
        clean_my_user cleanall_my_user

clean_nuclear_decay: 
	rm -f $(DECAY_OBJS)

cleanall_nuclear_decay: clean_nuclear_decay
	rm -f $(BINDIR)/$(DECAY_EXEC) $(BINDIR)/$(DECAY_EXEC).exe

clean_my_user:
	rm -f $(MY_USER_OBJ)

cleanall_my_user:
	rm -fr $(MY_USER_DIR)

#===============================================================================
# Define.
#===============================================================================

DECAY_DEF = yes
