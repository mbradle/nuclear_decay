////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute overabundances.  This assumes a fully decayed
//!        This assumes a fully decayed input file, though one may zero
//!        out abundances with a NucView.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <Libnucnet.h>
#include <WnSparseSolve.h>
#include <boost/lexical_cast.hpp>
#include <boost/unordered_map.hpp>

#include "nnt/auxiliary.h"
#include "nnt/iter.h"

//##############################################################################
// clean_up_abundances().
//##############################################################################

void
clean_up_abundances( nnt::Zone& zone, Libnucnet__NucView * p_zero_out_view )
{

  nnt::species_list_t species_list =
    nnt::make_species_list(
      Libnucnet__NucView__getNuc( p_zero_out_view )
    );

  BOOST_FOREACH( nnt::Species species, species_list )
  {

    Libnucnet__Zone__updateSpeciesAbundance(
      zone.getNucnetZone(),
      species.getNucnetSpecies(),
      0.
    );

  }

}

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char **argv )
{

  typedef boost::unordered_map<std::string, double> unordered_map;
  unordered_map map;
  unordered_map::iterator it;

  Libnucnet * p_my_nucnet, * p_solar;
  Libnucnet__Zone * p_solar_zone;
  double d_abund;
  Libnucnet__NucView * p_zero_out_view;

  //============================================================================
  // Check input.
  //============================================================================

  if( argc == 2 && strcmp( argv[1], "--example" ) == 0 )
  {
    std::cout << std::endl;
    std::cout << argv[0] << " fully_decayed_abundances.xml " <<
      "../nucnet-tools-code/data_pub/Lodders.xml " <<
      "\"[position() >= last() - 10]\"" <<
      std::endl << std::endl;
    exit( EXIT_FAILURE );
  }

  if( argc != 4 )
  {
    fprintf(
      stderr,
      "\nUsage: %s xml_file solar_xml zone_xpath\n\n",
      argv[0]
    );
    fprintf(
      stderr,
      "  xml_file = input full libnucnet xml file\n\n"
    );
    fprintf(
      stderr,
      "  solar_xml = xml file of solar abundances\n\n"
    );
    fprintf(
      stderr,
      "  zone_xpath = XPath expression to select zones\n\n"
    );
    std::cout << "For an example usage, type " << std::endl << std::endl;
    std::cout << argv[0] << " --example" << std::endl << std::endl;
    return EXIT_FAILURE;
  }

  //============================================================================
  // Input.
  //============================================================================

  p_my_nucnet =
    Libnucnet__new_from_xml(
      argv[1],
      "",
      "",
      argv[3]
    );

  p_solar = Libnucnet__new();

  Libnucnet__Net__updateFromXml(
    Libnucnet__getNet( p_solar ),
    argv[1],
    "",
    ""
  );

  Libnucnet__assignZoneDataFromXml( p_solar, argv[2], "" );

  //============================================================================
  // Set view of species to zero out.
  //============================================================================
  
  p_zero_out_view =
    Libnucnet__NucView__new(
      Libnucnet__Net__getNuc( Libnucnet__getNet( p_my_nucnet ) ),
      "[(z = 84 and a >= 213) or z > 84]"
    );
  
  //============================================================================
  // Populate map then clean up solar nucnet.
  //============================================================================

  p_solar_zone = Libnucnet__getZoneByLabels( p_solar, "0", "0", "0" );

  nnt::species_list_t solar_species_list =
    nnt::make_species_list(
      Libnucnet__Net__getNuc( Libnucnet__getNet( p_solar ) ) 
    );

  BOOST_FOREACH( nnt::Species species, solar_species_list )
  {

    d_abund =
      Libnucnet__Zone__getSpeciesAbundance(
        p_solar_zone,
        species.getNucnetSpecies()
      );

    if( d_abund )
    {

      map.insert(
        unordered_map::value_type(
          Libnucnet__Species__getName( species.getNucnetSpecies() ),
          d_abund
        )
      );

    }

  }

  Libnucnet__free( p_solar );

  //============================================================================
  // Iterate zones and store overabundances.
  //============================================================================

  nnt::species_list_t species_list =
    nnt::make_species_list(
      Libnucnet__Net__getNuc( Libnucnet__getNet( p_my_nucnet ) )
    );

  Libnucnet__setZoneCompareFunction(
    p_my_nucnet,
    (Libnucnet__Zone__compare_function)
      nnt::zone_compare_by_first_label
  );

  nnt::zone_list_t zone_list = nnt::make_zone_list( p_my_nucnet );

  BOOST_FOREACH( nnt::Zone zone, zone_list )
  {

    std::cout <<
      "Zone: " <<
      Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 ) <<
      "  " <<
      Libnucnet__Zone__getLabel( zone.getNucnetZone(), 2 ) <<
      "  " <<
      Libnucnet__Zone__getLabel( zone.getNucnetZone(), 3 ) <<
      "  " << std::endl << std::endl;

    clean_up_abundances( zone, p_zero_out_view );

    BOOST_FOREACH( nnt::Species species, species_list )
    {

      d_abund =
        Libnucnet__Zone__getSpeciesAbundance(
          zone.getNucnetZone(),
          species.getNucnetSpecies()
        );

      if( d_abund > 0. )
      {

        it =
          map.find( Libnucnet__Species__getName( species.getNucnetSpecies() ) );

        if( it == map.end() )
        {

          std::cerr << 
            Libnucnet__Species__getName( species.getNucnetSpecies() ) <<
            " does not occur in the solar abundances." << std::endl;
          return EXIT_FAILURE;

        }
        else
        {

          std::cout <<
            it->first <<
            "  " <<
            d_abund / it->second << std::endl;

        }

      }

    }

    std::cout << std::endl;
    
  }
    
  //============================================================================
  // Clean up and exit.
  //============================================================================

  Libnucnet__free( p_my_nucnet );

  return EXIT_SUCCESS;

}
